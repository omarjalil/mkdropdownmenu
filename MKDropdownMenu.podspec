Pod::Spec.new do |s|
  s.name             = "MKDropdownMenu"
  s.version          = "1.4"
  s.summary          = "Dropdown Menu for iOS"
  s.description      = <<-DESC
Dropdown Menu for iOS with many customizable parameters to suit any needs.
                       DESC

  s.homepage         = "https://bitbucket.org/omarjalil/mkdropdownmenu"
  s.screenshots      = "https://raw.github.com/maxkonovalov/MKDropdownMenu/master/Screenshots/MKDropdownMenu.png"
  s.license          = 'MIT'
  s.author           = "Max Konovalov"
  s.source           = { :git => "https://bitbucket.org/omarjalil/mkdropdownmenu.git" }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = '*.{h,m}'
end